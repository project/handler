<?php

/**
 * @file
 * Handler include classes and interfaces.
 */


interface HandlerInterface {
  
  public function setEnvironment(DrupalEnvironmentInterface $env);
  
}