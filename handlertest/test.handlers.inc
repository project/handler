<?php


interface FancystringInterface extends HandlerInterface {
  
  public function setString($string);
  
  public function getString();
}

class FancystringDefault implements FancystringInterface {
  
  protected $env;
  
  protected $string = '';
  
  public function setEnvironment(DrupalEnvironmentInterface $env) {
    $this->env = $env;
  }
  
  public function setString($string) {
    $this->string = $string;
  }
  
  public function getString() {
    return $this->string;
  }
  
}

class FancystringRot13 implements FancystringInterface {
  
  protected $env;
  
  protected $string = '';
  
  public function setEnvironment(DrupalEnvironmentInterface $env) {
    $this->env = $env;
  }
  
  public function setString($string) {
    $this->string = $string;
  }
  
  public function getString() {
    return str_rot13($this->string);
  }
  
}


class FancystringCustom implements FancystringInterface {
  
  protected $env;
  
  protected $string = '';
  
  public function setEnvironment(DrupalEnvironmentInterface $env) {
    $this->env = $env;
  }
  
  public function setString($string) {
    $this->string = $string;
  }
  
  public function getString() {
    $map = $this->env->variableGet('fancystring_translate', array());
    
    return strtr($this->string, $map);
  }
}


class FancyStringCustomTestEnv extends EnvironmentDefault {

  public function variableGet($var, $default) {
    
    if ($var == 'fancystring_translate') {
      return array(
        'a' => 'b',
        'b' => 'c',
        'c' => 'd',
      );
    }
    
    return variable_get($var, $default);
  }
}

