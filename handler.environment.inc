<?php

/**
 * @file
 * The Drupal environment uber variable.
 * 
 * Handlers by design should be totally insulated from the context in which
 * they run.  All communication eith their environment is through a provided
 * environment object, which manages communication with the rest of the system.
 * That makes it very easy to "mock" the environment in order to unit test
 * a handler object in a vaccuum.
 */

/**
 * The basic Drupal environment object interface.
 *
 */
interface DrupalEnvironmentInterface {
  
  public function variableGet($var, $default);
  public function variableSet($var, $default);
  
  public function httpGet($key = NULL, $default = NULL);
  public function httpPost($key = NULL, $default = NULL);
  public function httpCookie($key = NULL, $default = NULL);
  public function requestArgument($arg);
  public function requestPath();
  
  public function serverInfo($key = NULL, $default = NULL);
  public function envInfo($key = NULL, $default = NULL);
  
  public function invokeHooks($hook);
  public function handler($target, $options = array());
  
}

class EnvironmentDefault implements DrupalEnvironmentInterface {
  
  protected $requestPath;
  protected $getArray;
  protected $postArray;
  protected $cookieArray;
  protected $serverArray;
  protected $envArray;
  
  public function __construct() {
    $this->requestPath = drupal_get_normal_path($_GET['q']);
    
    $this->getArray = $_GET;
    $this->postArray = $_POST;
    $this->cookieArray = $_COOKIE;
    $this->serverArray = $_SERVER;
    $this->envArray = $_ENV;
    unset($this->getArray['q']);
    
    $this->requestArguments = explode('/', $this->requestPath);
    
  }
  
  public function variableGet($var, $default) {
    return variable_get($var, $default);
  }
  
  public function variableSet($var, $default) {
    return variable_set($var, $default);
  }
  
  public function requestPath() {
    return $this->requestPath;
  }
  
  protected function arrayIndexLookup($array, $key = NULL, $default = NULL) {
    if (isset($key)) {
      return isset($this->$array[$key]) ? $this->$array[$key] : $default;
    }
    else {
      return $this->$array;
    }
  }
  
  public function httpGet($key = NULL, $default = NULL) {
    return $this->arrayIndexLookup('getArray', $key, $default);
  }
  
  public function httpPost($key = NULL, $default = NULL) {
    return $this->arrayIndexLookup('postArray', $key, $default);
  }
  
  public function httpCookie($key = NULL, $default = NULL) {
    return $this->arrayIndexLookup('cookieArray', $key, $default);
  }
  
  public function serverInfo($key = NULL, $default = NULL) {
    return $this->arrayIndexLookup('serverArray', $key, $default);
  }
  
  public function envInfo($key = NULL, $default = NULL) {
    return $this->arrayIndexLookup('envArray', $key, $default);
  }
  
  public function requestArgument($arg) {
    return isset($this->requestArguments[$arg]) ? $this->requestArguments[$arg] : NULL;
  }
  
  public function invokeHooks($hook) {
    return call_user_func_array('module_invoke_all', func_get_args());
  }
  
  public function handler($target, $options = array()) {
    return handler($target, $options);
  }
}

